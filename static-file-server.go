package main

import (
	"net/http"
	"os"
	"fmt"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "8080"
	}

	http.Handle("/", http.FileServer(http.Dir("./public")))

	fmt.Println("Listen on localhost:"+port)

	http.ListenAndServe(":"+port, nil)
}

# GolangStaticFileServer

Golang static file server example for return `*.html` or `*.js`, `*.css`, `*.*` files

##### Local run
```bash
go run static-file-server.go
```

##### Resources:
- [Serving Static Files with HTTP by Golang Wiki](https://github.com/golang/go/wiki/HttpStaticFiles)
- [Serving Static Sites with Go by Alex Edwards](https://www.alexedwards.net/blog/serving-static-sites-with-go)
- [Simple Static File Server in Go](https://gist.github.com/paulmach/7271283)

##### Free golang hosting
- [Heroku](https://gitlab.com/SenseyeFreeHostTemplate/HerokuGolangFileServer)
- [Bluemix (IBM)](https://gitlab.com/SenseyeFreeHostTemplate/BluemixGolangFileServer)